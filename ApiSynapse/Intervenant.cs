﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiSynapse
{
    public class Intervenant
    {
        public short Id { get; private set; }
        public string Nom { get; set; }
        public decimal TauxHoraire { get; set; }
        private bool isNew = true;
        public Intervenant()
        {
            Nom = "";
            TauxHoraire = 0;
        }
        private static string _selectSql = "SELECT id, nom, tauxhoraire FROM intervenant";
        private static string _selectByIdSql = "SELECT id, nom, tauxhoraire FROM intervenant WHERE id=?id";

        public static List<Intervenant> FestchAll()
        {
            List<Intervenant> resultat = new List<Intervenant>(); //variable résultat de type collection d'intervenant
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection(); //Déclaration et initialisation d'un objet de connexion
            MySqlCommand commandSql = openConnection.CreateCommand();//Déclaration et initialisation d'un objet permettant d'interroger la base de données
            commandSql.CommandText = _selectSql;//La représentation textuelle de la requête SQL est transmise à l'objet en charge d'interroger la base de données
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête SQL
            while (jeuEnregistrements.Read())//Le jeu d'enregistrement contenant le résultat de la requête est parcouru
            {
                Intervenant unIntervenant = new Intervenant();//à chaque itération un intervenant est crée et valorisé

                string idIntervenant = jeuEnregistrements["id"].ToString();
                unIntervenant.Id = Convert.ToInt16(idIntervenant);
                unIntervenant.Nom = jeuEnregistrements["nom"].ToString();

                string tauxHoraire = jeuEnregistrements["tauxHoraire"].ToString();
                unIntervenant.TauxHoraire = Convert.ToDecimal(tauxHoraire);
                unIntervenant.isNew = false;//L'intervenant n'est pas nouveau dans le contexte applicatif puisqu'il provient de la base de données
                resultat.Add(unIntervenant);//L'intervenant valorisé à partir des informations de la base de données est ajouté à la collection
            }
            openConnection.Close();//La connexion est fermée
            return resultat;
        }


        public static Intervenant Fetch(int idIntervenant)
        {
            Intervenant unIntervenant = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requête à utiliser lors de l'envoie de la requête. Il s'agit ici de l'identifiant transmis en paramètre
            commandSql.Parameters.Add(new MySqlParameter("?id", idIntervenant));//Transmet un paramètre à utiliser lors de l'envoie de la requête. Il s'agit ici de l'identifiant transmis en paramètre
            commandSql.Prepare();//Prépare la requête. Il s'agit ici de l'identifiant transmis en paramètre
            MySqlDataReader jeuEnregistrement = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrement.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                unIntervenant = new Intervenant();//Initailisation de la variable Contact
                unIntervenant.Id = Convert.ToInt16(jeuEnregistrement["id"].ToString());//Lecture d'un champ de l'enregistrement
                unIntervenant.Nom = jeuEnregistrement["Nom"].ToString();
                string tauxHoraire = jeuEnregistrement["tauxHoraire"].ToString();
                unIntervenant.TauxHoraire = Convert.ToDecimal(tauxHoraire);
                unIntervenant.isNew = false;
            }
            openConnection.Close();
            return unIntervenant;


        }

        private static string _updateSql = "UPDATE intervenant SET nom=?nom, tauxHoraire=?tauxHoraire WHERE id=?id";
        private static string _insertSql = "INSERT INTO intervenant (nom,tauxHoraire) VALUES (?nom,?tauxHoraire)";

        private void Insert()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(new MySqlParameter("?tauxHoraire", TauxHoraire));
            commandSql.Parameters.Add(new MySqlParameter("?nom", Nom));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            Id = Convert.ToInt16(commandSql.LastInsertedId);

            openConnection.Close();
        }

        private void Update()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _updateSql;

            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Parameters.Add(new MySqlParameter("?tauxHoraire", TauxHoraire));
            commandSql.Parameters.Add(new MySqlParameter("?nom", Nom));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            openConnection.Close();
        }

        public void Save()
        {
            if (isNew)
            {
                Insert();
            }
            else
            {
                Update();
            }

        }

    }
}