using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace ApiSynapse
{
    public class Projet
    {
        /// <summary>
        /// Identifiant du projet
        /// </summary>
        public short Id { get; private set; } // Primary Key (Bd)
        /// <summary>
        /// Nom du projet
        /// </summary>
        public string NomProjet { get; set; }
        /// <summary>
        /// Date de début du projet
        /// </summary>
        public DateTime DateDebutProjet { get; set; }
        /// <summary>
        /// Date de fin du projet
        /// </summary>
        public DateTime DateFinProjet { get; set; }
        /// <summary>
        /// Prix facturé
        /// </summary>
        public decimal PrixFactureMO { get; set; }
        /// <summary>
        /// Missions réalisées dans le cadre du projet
        /// </summary>
        private List<Mission> Missions;

        private bool isNew = true;

        /// <summary>
        /// Ajoute une mission au projet
        /// </summary>
        /// <param name="uneMission"></param>
        public void AjouterMission(Mission uneMission)
        {
            if (!Missions.Exists(m => m.Id == uneMission.Id))
            {
                /*
				* Partie à compléter
				*/
            }
        }

        /// <summary>
        /// Supprime une mission du projet
        /// </summary>
        /// <param name="uneMission"></param>
        public void SupprimerMission(Mission uneMission)
        {
				/*
				* Partie à compléter
				*/
        }

        /// <summary>
        /// Initialise un nouveau projet
        /// </summary>
        public Projet()
        {            
				/*
				* Partie à compléter
				*/
        }

        /// <summary>
        /// Retourne un projet
        /// </summary>
        /// <param name="idProjet">L'identifiant d'un projet</param>
        /// <returns>Une référence de projet ou null si l'identifiant ne correspond à aucun projet</returns>                  
        public static Projet Fetch(int idProjet)
        {
            Projet unProjet = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", idProjet));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                unProjet = new Projet();
                unProjet.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());
                unProjet.NomProjet = jeuEnregistrements["nomProjet"].ToString();
                long dateDebutProjet = Convert.ToInt64(jeuEnregistrements["dateDebutProjet"].ToString());
                unProjet.DateDebutProjet = UnixTimeStampToDateTime(dateDebutProjet);
                long dateFinProjet = Convert.ToInt64(jeuEnregistrements["dateFinProjet"].ToString());
                unProjet.DateFinProjet = UnixTimeStampToDateTime(dateFinProjet);
                string prixFactureMO = jeuEnregistrements["prixFacture"].ToString();
                unProjet.PrixFactureMO = Convert.ToDecimal(prixFactureMO);
                unProjet.Missions = Mission.FetchAllByProjet(unProjet);
            }
            openConnection.Close();
            return unProjet;
        }

        /// <summary>
        /// Retourne l'ensemble des projets de l'entreprise 
        /// </summary>
        /// <returns>Une collection de projets</returns>
        public static List<Projet> FetchAll()
        {
            List<Projet> projets = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectSql;
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Projet unProjet = new Projet();
                unProjet.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());
                unProjet.NomProjet = jeuEnregistrements["nomProjet"].ToString();
                long dateDebutProjet = Convert.ToInt64(jeuEnregistrements["dateDebutProjet"].ToString());
                unProjet.DateDebutProjet = UnixTimeStampToDateTime(dateDebutProjet);
                long dateFinProjet = Convert.ToInt64(jeuEnregistrements["dateFinProjet"].ToString());
                unProjet.DateFinProjet = UnixTimeStampToDateTime(dateFinProjet);
                string prixFactureMO = jeuEnregistrements["prixFacture"].ToString();
                unProjet.PrixFactureMO = Convert.ToDecimal(prixFactureMO);
                unProjet.Missions = Mission.FetchAllByProjet(unProjet);
                projets.Add(unProjet);
            }
            openConnection.Close();
            return projets;
        }

        /// <summary>
        /// Sauvegarde le projet courant
        /// </summary>
        public void Save()
        {
            if (isNew)
            {
                Insert();
            }
            else
            {
                Update();

            }
            SaveMission();
        }

        private void SaveMission()
        {
            /*
			* Partie à compléter
			*/
        }

        private void Update()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Parameters.Add(new MySqlParameter("?dateDebutProjet", DateTimeToUnixTimeStamp(DateDebutProjet)));
            commandSql.Parameters.Add(new MySqlParameter("?nomProjet", NomProjet));
            commandSql.Parameters.Add(new MySqlParameter("?dateFinProjet", DateTimeToUnixTimeStamp(DateFinProjet)));
            commandSql.Parameters.Add(new MySqlParameter("?prixFacture", PrixFactureMO));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();

        }

        private void Insert()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertSql;
            commandSql.Parameters.Add(new MySqlParameter("?dateDebutProjet", DateTimeToUnixTimeStamp(DateDebutProjet)));
            commandSql.Parameters.Add(new MySqlParameter("?nomProjet", NomProjet));
            commandSql.Parameters.Add(new MySqlParameter("?dateFinProjet", DateTimeToUnixTimeStamp(DateFinProjet)));
            commandSql.Parameters.Add(new MySqlParameter("?prixFacture", PrixFactureMO));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            Id = Convert.ToInt16(commandSql.LastInsertedId);
            openConnection.Close();
        }

        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        private static long DateTimeToUnixTimeStamp(DateTime dateTime)
        {
            return ((DateTimeOffset)dateTime).ToUnixTimeSeconds();
        }

        private static string _selectSql =
          "SELECT id ,  nomProjet ,  dateDebutProjet, dateFinProjet, prixFacture FROM projet";

        private static string _selectByIdSql =
            "SELECT id ,  nomProjet ,  dateDebutProjet, dateFinProjet, prixFacture FROM projet WHERE id = ?id ";

        private static string _updateSql =
            "UPDATE projet set nomProjet=?nomProjet , dateDebutProjet=?dateDebutProjet, dateFinProjet=?dateFinProjet, prixFacture=?prixFacture  WHERE id=?id ";

        private static string _insertSql =
            "INSERT INTO projet (nomProjet,dateDebutProjet,dateFinProjet,prixFacture) VALUES (?nomProjet,?dateDebutProjet,?dateFinProjet,?prixFacture)";

    }
}
